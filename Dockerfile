# hadolint ignore=DL3007
FROM python:3-alpine

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF
ARG TARGETPLATFORM

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="terraform-inventory" \
    org.opencontainers.image.description="A lightweight automatically updated alpine python image with opentofu and state2inventory" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.version="${VERSION}" \
    org.opencontainers.image.url="https://hub.docker.com/r/jfxs/terraform-inventory" \
    org.opencontainers.image.source="https://gitlab.com/op_so/docker/terraform-inventory" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY files/uid-entrypoint.sh /usr/local/bin/

COPY ot-dist/${VERSION}/$TARGETPLATFORM/tofu /usr/local/bin/

# hadolint ignore=DL3013,DL3018
RUN apk --no-cache add ca-certificates curl git jq \
    && chmod 755 /usr/local/bin/uid-entrypoint.sh \
    && chmod 755 /usr/local/bin/tofu \
    && sh -c "$(wget -qO - https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin \
    && pip install --no-cache-dir state2inventory \
    && chmod -R g=u /etc/passwd

COPY files/uid-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["uid-entrypoint.sh"]

USER 10010
CMD ["tofu", "--version"]
