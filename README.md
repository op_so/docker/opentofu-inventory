<!-- vale off -->
# Docker OpenTofu Inventory
<!-- vale on -->

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=flat)](LICENSE)
[![Pipeline Status](https://gitlab.com/op_so/docker/opentofu-inventory/badges/main/pipeline.svg)](https://gitlab.com/op_so/docker/opentofu-inventory/pipelines)

An [OpenTofu](https://opentofu.org/) Docker image:

* **lightweight** image based on Alpine Linux only #50 MB,
* `multiarch` with support of **amd64** and **arm64**,
* **non-root** container user,
* **automatically** updated by comparing software bill of materials (`SBOM`) changes,
* image **signed** with [Cosign](https://github.com/sigstore/cosign),
* a **software bill of materials (`SBOM`) attestation** added using [`Syft`](https://github.com/anchore/syft),
* available on **Docker Hub** and **Quay.io**.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/docker/opentofu-inventory) The main repository.

[![Docker Hub](https://shields.io/badge/dockerhub-informational?logo=docker&logoColor=white&style=flat-square)](https://hub.docker.com/r/jfxs/opentofu-inventory) The Docker Hub registry.

[![Quay.io](https://shields.io/badge/quay.io-informational?logo=docker&logoColor=white&style=flat-square)](https://quay.io/repository/ifxs/opentofu-inventory) The Quay.io registry.

This image also includes a Python program [`state2inventory`](https://gitlab.com/op_so/ansible/state2inventory) to generate an [`Ansible`](https://www.ansible.com/) inventory from an infrastructure state.

<!-- vale off -->
## Running OpenTofu and state2inventory
<!-- vale on -->

```shell
docker run -t --rm jfxs/opentofu-inventory tofu version
docker run -t --rm jfxs/opentofu-inventory state2inventory terraform --help
```

or

```shell
docker run -t --rm quay.io/ifxs/opentofu-inventory tofu version
docker run -t --rm quay.io/ifxs/opentofu-inventory state2inventory terraform --help
```

## Built with

Docker latest tag is [--VERSION--](https://gitlab.com/op_so/docker/opentofu-inventory/-/blob/main/Dockerfile) and has:

<!-- vale off -->
--SBOM-TABLE--
<!-- vale on -->

[`Dockerhub` Overview page](https://hub.docker.com/r/jfxs/opentofu-inventory) has the details of the last published image.

## Versioning

Docker tag definition:

* the OpenTofu version used,
* a dash
* an increment to differentiate build with the same version starting at 001

```text
<opentofu_version>-<increment>
```

<!-- vale off -->
Example: 1.6.0-003
<!-- vale on -->

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the software bill of materials (`SBOM`) attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
